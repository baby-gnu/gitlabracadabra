# Change Log

## [1.12.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.11.0...v1.12.0) (2023-01-17)


### :repeat: Chore

* **deps:** do not pin flake8-rst-docstrings ([74a52bc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/74a52bc43127f1b319a58cb15aaeb93f04457220))
* **deps:** update dependency flake8-bugbear to v22.10.27 ([cb13eb4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cb13eb4a7d412cff67ee1f92ce9de2c6a6cdeff2))
* **deps:** update dependency flake8-bugbear to v22.10.27 ([394e81a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/394e81a8c7c7f7a52799618ff85bdee0628c8a8e))
* **deps:** update dependency flake8-bugbear to v22.12.6 ([ea67bb5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ea67bb5b1d98af3d2880606bfca9907b66cf6b36))
* **deps:** update dependency flake8-bugbear to v22.12.6 ([46766ba](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/46766ba07c74992c49fcefdc888ad26a6fbc2057))
* **deps:** update dependency flake8-builtins to v2.0.1 ([0449657](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0449657c98be6d0179f1c17ed3e4920a5eb6ebaf))
* **deps:** update dependency flake8-builtins to v2.0.1 ([846d2f1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/846d2f1deaf39bfb4de2466961e53ada3632796f))
* **deps:** update dependency flake8-builtins to v2.1.0 ([b521bd7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b521bd7c5f4c8731681cae12f707a79286a2df4b))
* **deps:** update dependency flake8-builtins to v2.1.0 ([70f670b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/70f670be5f56140d736e98f3b0ad758ed089d1dd))
* **deps:** update dependency flake8-comprehensions to v3.10.1 ([730c608](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/730c608ea3d5b7ee677402ec55178b6a42d00005))
* **deps:** update dependency flake8-comprehensions to v3.10.1 ([9bb1d4e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9bb1d4ef28c235ee1984a6fe8e49a7fc5071bfc4))
* **deps:** update dependency flake8-logging-format to v0.9.0 ([a57f4fa](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a57f4fa250cdca2439501777cbce45a52c341d02))
* **deps:** update dependency flake8-logging-format to v0.9.0 ([9d9500b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9d9500b0ae7d28bf691a0f17ce5b4971fec4b171))
* **deps:** update dependency flake8-quotes to v3.3.2 ([258abac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/258abac7c035193e1d0b5cf6dac93b3c1f8220a8))
* **deps:** update dependency flake8-quotes to v3.3.2 ([fef7b5e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fef7b5eeea1746c32b87b74f07a008eb8e1f21aa))
* **deps:** update dependency flake8-rst-docstrings to v0.3.0 ([6284d8a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6284d8a1a00864d5c1c22310b012c1f2ad4bf732))
* **deps:** update dependency flake8-rst-docstrings to v0.3.0 ([53b7921](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/53b7921f64a7b2ce4a4cf95c36a5c96883ebcc09))
* **deps:** update dependency isort to v5.11.1 ([86ab591](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/86ab591df29d4a78547df06c5db6aae75fbe821c))
* **deps:** update dependency isort to v5.11.1 ([4fc6d1a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4fc6d1a629b3657811a48ddccf671112f17bfcd9))
* **deps:** update dependency isort to v5.11.3 ([f883955](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f883955ed4c445cda463a7b33d45f0c34107bf7d))
* **deps:** update dependency isort to v5.11.3 ([c108dfd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c108dfd33d64cd1f71a349a9492608e00157fb44))
* **deps:** update dependency isort to v5.11.4 ([4f22313](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4f223135626786e43aa8de594d99397f35629efb))
* **deps:** update dependency isort to v5.11.4 ([8b22d16](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8b22d16f9db2e724dbceaf141d06fa467efda20f))
* **deps:** update dependency jsonschema to v4.17.0 ([20d7e24](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/20d7e24158fcb4d36913c3c1f35eef3d9f653aef))
* **deps:** update dependency jsonschema to v4.17.0 ([a035368](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a0353683b06d668f31c056c808706f26ecc32868))
* **deps:** update dependency jsonschema to v4.17.1 ([f57320d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f57320dad55c26b9acc64f0604c251b957e01bce))
* **deps:** update dependency jsonschema to v4.17.1 ([250370d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/250370df5b9c9824ea9dff3735d52a2d0ccb3774))
* **deps:** update dependency jsonschema to v4.17.3 ([9a4f3db](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9a4f3dbc1e0f0158aa9c1f2a578c2cab0bb281ec))
* **deps:** update dependency jsonschema to v4.17.3 ([714e2a9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/714e2a9fc846a4756880f16b3632ec50fda6c89b))
* **deps:** update dependency mypy to v0.990 ([a8d0e59](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a8d0e59f924ad5e9a9b429e159ce1a5b6ce6ecc4))
* **deps:** update dependency mypy to v0.990 ([2a43654](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2a436545ce5824616e9bf44f287fbd5314b5b0ce))
* **deps:** update dependency mypy to v0.991 ([35c3351](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/35c3351eec9ec42468cd6db28c0b1e1342cce163))
* **deps:** update dependency mypy to v0.991 ([5996388](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/59963889c8467e1a63612bd8284a2a12ec9ffc3f))
* **deps:** update dependency packaging to v22 ([f4c3ef0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f4c3ef0b87e2c9f405cd0c5f3527be610dbaaf3d))
* **deps:** update dependency packaging to v22 ([b0a4a79](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b0a4a798cfb7630c2ef1dd6337a30473a5edea6e))
* **deps:** update dependency packaging to v23 ([ba1c435](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ba1c4359b6a31d2ca65076edbfe2e1daa96329ec))
* **deps:** update dependency packaging to v23 ([611d52a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/611d52a665e69215adbe61ada5e53e062383111a))
* **deps:** update dependency pygithub to v1.57 ([8f1e6ee](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8f1e6ee7a4a4550ad74262c183dadc86e39bb227))
* **deps:** update dependency pygithub to v1.57 ([923560f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/923560fb0caf59e4dea9ac6c44769d02e08ddae7))
* **deps:** update dependency pylint to v2.15.10 ([ad6607f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ad6607f8ec884a09a02ed56ff3a6dc1a96a4bd3d))
* **deps:** update dependency pylint to v2.15.10 ([be7a14c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/be7a14ccb5cf68fe94944713aac3f48793d5e17a))
* **deps:** update dependency pylint to v2.15.6 ([a65325b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a65325b435dd8400ca141dc4ac233677ac2fe882))
* **deps:** update dependency pylint to v2.15.6 ([2642f50](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2642f50d2b5c945207501dfedf38218e7bf8a17a))
* **deps:** update dependency pylint to v2.15.8 ([4b6016b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4b6016b9f98cbff75b983dd48cbd410d933c72a9))
* **deps:** update dependency pylint to v2.15.8 ([22ab38f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/22ab38ff6437187503e707e7ca29185320b41735))
* **deps:** update dependency pylint to v2.15.9 ([d5ba390](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d5ba390c71da9f087e7eef4f4a5a8b326d26c9ee))
* **deps:** update dependency pylint to v2.15.9 ([a9bfeb1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a9bfeb1afb478bbd86e94c86d40a0568a9c56290))
* **deps:** update dependency python-gitlab to v3.11.0 ([da19dab](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/da19dabce3fe88e0f6ca97aa545f3f0b136abb2f))
* **deps:** update dependency python-gitlab to v3.11.0 ([bdd12ca](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bdd12cabd15a1241091e5395e2a0a633bf8112f7))
* **deps:** update dependency python-gitlab to v3.12.0 ([0d4b25e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0d4b25ee97c14d54b0bd3db8422b9cf0aee5035c))
* **deps:** update dependency python-gitlab to v3.12.0 ([88a683b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/88a683bd2dfbe75ef23df3c2ae9e8d7dde21c01e))
* **deps:** update dependency types-html5lib to v1.1.11.1 ([dc76943](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dc769431e7e8ab9cbaacdc868ce82028ea8680a1))
* **deps:** update dependency types-html5lib to v1.1.11.1 ([7ed6951](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7ed695142e5bb64ed82823baaa461f6c723b3df8))
* **deps:** update dependency types-html5lib to v1.1.11.10 ([ff59b3e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ff59b3ec04e07301a92d7bb96855ef120de003ee))
* **deps:** update dependency types-html5lib to v1.1.11.10 ([e2513ba](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e2513baedea1b7243606575e4678e909d86ae3a7))
* **deps:** update dependency types-pyyaml to v6.0.12.1 ([ac287af](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ac287afe593d0c35139487a679d10b48c932746c))
* **deps:** update dependency types-pyyaml to v6.0.12.1 ([87b8016](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/87b8016664f26abb601f16ad6d647ef7cba58215))
* **deps:** update dependency types-pyyaml to v6.0.12.2 ([b44c4ed](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b44c4edd3cebe64eb4069960318569fcd83bb5d4))
* **deps:** update dependency types-pyyaml to v6.0.12.2 ([4ac2d14](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4ac2d14606461485aa1e4435cf369babf3542cf4))
* **deps:** update dependency types-requests to v2.28.11.4 ([844844f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/844844f4769366cb884a4e9075048d29ebf571c4))
* **deps:** update dependency types-requests to v2.28.11.4 ([4c981fd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4c981fdcd5985854ef1f8b0ac72b6728475b083b))
* **deps:** update dependency types-requests to v2.28.11.5 ([dbff192](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dbff192549e133763d70218adbdeafaff6a84803))
* **deps:** update dependency types-requests to v2.28.11.5 ([9a98db0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9a98db094e8a6a5de52607b89119addc36002030))
* **deps:** update dependency types-requests to v2.28.11.7 ([2974984](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/297498491b979378e3636fa340e3a028741d554c))
* **deps:** update dependency types-requests to v2.28.11.7 ([283c4a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/283c4a3be662401cdaaf5af7db4b2e0067c13a75))
* **deps:** update node.js to v19.1.0 ([5726125](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5726125be3ffeb34dc6281f91d60b30d04c354f7))
* **deps:** update node.js to v19.1.0 ([0c8712b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0c8712bcb20ca4e312f6779a746918fcdf5059fe))
* **deps:** update node.js to v19.2.0 ([c46c927](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c46c92705e75956dfcab06eb028d07a432b92bf5))
* **deps:** update node.js to v19.2.0 ([613e82d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/613e82d6c2f84380a6a264cb0580519e6b1fa145))
* **deps:** update node.js to v19.3.0 ([669a947](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/669a947385d685068ac74020cad14d7c65e2c0c3))
* **deps:** update node.js to v19.3.0 ([5e15643](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5e156432565086779fd360085f4ec398a0f3c664))
* **deps:** update node.js to v19.4.0 ([7c6ee3d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7c6ee3d5c1720a03306bb46b83deba2229518074))
* **deps:** update node.js to v19.4.0 ([b3fe0a7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b3fe0a7736fd29558361e6b8619459f4df1617e5))
* **gitlab:** do not pass None password ([a32fc4b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a32fc4bb10a4a5f4e84f2eba3fafceeae0e9b4d9))


### :sparkles: Features

* **github:** token thru GITHUB_TOKEN env ([1ef9b54](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1ef9b54415b4765edad4c6fcf1f698b1d959b7d3))
* **github:** token thru GITHUB_TOKEN env ([3257767](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/325776743ef5d9cf562a3a36aa9e0192db00c640))


### :memo: Documentation

* **release:** use scoped npm packages ([e8bfaa0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e8bfaa0f79da1ccc7e650ff4879fb9039abbc5a9))

## [1.11.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.10.0...v1.11.0) (2022-10-26)


### :bug: Fixes

* **debian:** fix repeated-path-segment doc ([9b42c94](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9b42c947385bb847febe216e2d76c3c11de96caa))


### :sparkles: Features

* **image_mirrors:** add enabled parameter ([c7c303b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c7c303b47dd2dfee6db9b8f6154092836d06982c))
* **pacakge_mirrors,image_mirrors:** retry backoff ([bbe4427](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bbe4427da1c47e34c9ae08e568576ec4a69f5d97))
* **pacakge_mirrors,image_mirrors:** retry backoff ([01f81b7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/01f81b70d9003961963e306c3014fe46ffbba467))
* **package_mirrors,image_mirrors:** add enabled parameter ([6b40466](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6b40466d3a77286bcc8ed5d6f772222b2f6b7b81))
* **package_mirrors:** add enabled parameter ([c9b9714](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c9b97147dc36a18b2e95d0c9a840c1e384c34804))


### :repeat: Chore

* **ci:** make most jobs manual ([9e2a688](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9e2a688efc5ffaafd620c08e34640e940de7c87b))
* **ci:** make most jobs manual ([b6c6288](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b6c62887239a19d7d8e784545797f8b19d704b10))
* **copyright:** update copyright to 2022 ([d34b1b4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d34b1b4fa0e7ebc0717cd31d889c8f4d5edfebdb))
* **copyright:** update copyright to 2022 ([83ce01f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/83ce01f514b5c74952fe42934b23e3450a8d16c6))
* **copyright:** update copyright to 2022 ([932f029](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/932f029fe22b519bfcceb20dfe090fdfd0be7c33))
* **copyright:** update copyright to 2022 and other packaging fixes ([98bc8d7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/98bc8d72b106e4c695cb199894e0326f29888ceb))
* **debian:** policy 4.6.1 ([72bcf73](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/72bcf73bb00382550241124f3d6a70fec65bf535))
* **deps:** do not pin wemake-python-styleguide dependencies ([1da7dac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1da7dacfb86fd16880018f2b18c4e33ecf5a89b4))
* **deps:** do not pin wemake-python-styleguide dependencies ([3540d13](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3540d13fa3d46008936de988e2a0b46689893706))
* **deps:** update dependency flake8-broken-line to v0.5.0 ([b59b113](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b59b1133d9023e037c530f85a427ef6a362a347c))
* **deps:** update dependency flake8-bugbear to v22.10.25 ([695113e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/695113e06cccf48767662bf43fe15f5e04645b2c))
* **deps:** update dependency flake8-bugbear to v22.10.25 ([71d6799](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/71d679929c87053ac82659bb8a5318806b293dbd))
* **deps:** update dependency flake8-builtins to v2 ([7e45dde](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7e45ddecd16a33962369cfb33db12c8c3fe8500f))
* **deps:** update dependency flake8-builtins to v2 ([7200934](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7200934d148ee7fb7f6ce236ad60122421d70213))
* **deps:** update dependency flake8-deprecated to v2 ([a5602ee](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a5602ee7d5047a256d29df744d51b1bdc7a0f08b))
* **deps:** update dependency flake8-deprecated to v2 ([638c00a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/638c00ac14c6a4ac62159aac7002c361b25a28fd))
* **deps:** update dependency flake8-logging-format to v0.8.1 ([c17f8ce](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c17f8ce53d3b25074f77768a507de64b59dc2c9a))
* **deps:** update dependency flake8-logging-format to v0.8.1 ([1419631](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/14196311b675494c53c97c2da54fefcfbcd9876c))
* **deps:** update dependency mypy to v0.981 ([0bd191b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0bd191bdd81900a5ad1e218f8a745e3f5ee226b4))
* **deps:** update dependency mypy to v0.981 ([603aca0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/603aca02152253fd164e213d6ef5d299e542befb))
* **deps:** update dependency mypy to v0.982 ([80d4898](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/80d48983ddd0be593d89c11edd1e59bf50a174ba))
* **deps:** update dependency mypy to v0.982 ([14ec802](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/14ec80269945b6bd46b8fca08ef89a134ddaa85d))
* **deps:** update dependency pygit2 to 1.10.1 ([16dcc31](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/16dcc3177e788f15098997a1cbe0e37ea8e385d6))
* **deps:** update dependency pygit2 to 1.10.1 ([f17eb10](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f17eb10eaf4c75b2dec3cbdfee0104ad190cf2f5))
* **deps:** update dependency pygithub to v1.56 ([6f80f9d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6f80f9d0f130e254e8397eeb9bf97608efe9bd2d))
* **deps:** update dependency pygithub to v1.56 ([7b25bff](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7b25bff6040811cc3de2d0b6a1ab840e3febc5b8))
* **deps:** update dependency pylint to v2.15.4 ([add9d52](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/add9d5261a125057031b3e2d1b3a28c350882e7f))
* **deps:** update dependency pylint to v2.15.4 ([9b8a2c9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9b8a2c96a7bf9dadd463ec367be82ed1720ee4ac))
* **deps:** update dependency pylint to v2.15.5 ([c4a9e16](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c4a9e1699d8970cb7ddaeb74533502ccdeb98bb4))
* **deps:** update dependency pylint to v2.15.5 ([f72cfb1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f72cfb1c9d61e7a18ccae9f37417c5ccb2d85038))
* **deps:** update dependency pytest to v7.2.0 ([eb1b389](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eb1b38955c0972f6cbd5cadf1de07c15c4d14f15))
* **deps:** update dependency pytest to v7.2.0 ([0bf7e87](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0bf7e8730cd324c3db1bcee0df89abe5cb33c0c7))
* **deps:** update dependency python-gitlab to v3.10.0 ([2e7064a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2e7064a400940d079d400ce889f4f21ca38a15cb))
* **deps:** update dependency python-gitlab to v3.10.0 ([5dec8ea](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5dec8ea4a008d491b0c7f7c449736eb2c627cde5))
* **deps:** update dependency types-pyyaml to v6.0.12 ([eb33ccb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eb33ccbb2c67e4fe8eff2dea371a474466c669d0))
* **deps:** update dependency types-pyyaml to v6.0.12 ([9b279a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9b279a3c88889c2364d8b07e17ea2634680120db))
* **deps:** update dependency types-requests to v2.28.11 ([b8d47ae](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b8d47ae225cfc356d8954e94337cd18c00676675))
* **deps:** update dependency types-requests to v2.28.11 ([9fef0e9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9fef0e99ee8fd80a56fe9d51e55df321bdb11a7c))
* **deps:** update dependency types-requests to v2.28.11.1 ([6ad2867](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6ad2867a0090be092c761ca6cfb420ff12883a19))
* **deps:** update dependency types-requests to v2.28.11.1 ([fe57f7c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fe57f7cb7c5cedf315852b38c79fab3cc88911bd))
* **deps:** update dependency types-requests to v2.28.11.2 ([a3aad5b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a3aad5b08eff98231193a4d7ceefc8e9f0e5d6d7))
* **deps:** update dependency types-requests to v2.28.11.2 ([61351a0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/61351a09676122aca654b5797838936191e7c4c1))
* **deps:** update dependency wemake-python-styleguide to v0.17.0 ([2db3993](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2db39935f3a846412192023e9bc7c7aa9d4161de))
* **deps:** update dependency wemake-python-styleguide to v0.17.0 ([351440d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/351440d29f70db2e396147365c0ff23e0e2a9a4d))
* **deps:** update node.js to v18.10.0 ([33819e0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/33819e07f40ca26b7f30489e2a2df448a2aceadf))
* **deps:** update node.js to v18.10.0 ([9704327](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9704327d97a51a51bfc2f3eeab5d8eab47fa5699))
* **deps:** update node.js to v18.11.0 ([7b31419](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7b31419c634f5ba684f6cd47975b1809e02511b2))
* **deps:** update node.js to v18.11.0 ([27c5ff7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/27c5ff771cafb8559667afa077489d60576c621a))
* **deps:** update node.js to v19 ([ba4ee4b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ba4ee4bc2250b26f5f8657ead62d68b1d61b75ae))
* **deps:** update node.js to v19 ([2add4f3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2add4f38da79bcce157095ae6423468f4ed175ee))
* **renovate:** ensure renovate catch pygit2 updates ([9024df2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9024df2568039ac15952aa848356afd79fa9d420))
* **typing:** removing two obsole type: ignore ([b738fad](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b738fad723ce6f04ee61617830bc99973e1437e1))


### :memo: Documentation

* improve release docs ([7682e47](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7682e47410a80fb319e26eeddb7e1badd999a410))
* two spelling fixes ([449d302](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/449d3025d1e7c7b9fabbf2f0e9851d99503dc9a2))
* update release.md ([643427e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/643427e32d6509e3f0e8f86bc7a577622b02c739))

## [1.10.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.9.1...v1.10.0) (2022-10-01)


### :sparkles: Features

* use pytest for tests ([78f0649](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/78f0649add1848dc1bf3495e0728e08b0e8dbc07)), closes [#1020992](https://gitlab.com/gitlabracadabra/gitlabracadabra/issues/1020992)


### :bug: Fixes

* **warnings:** fix fixture class name ([1564f3c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1564f3ca4d526eb8e7b345ff7dcc9a225f549424))
* **warnings:** fix fixture class name ([d79bb6d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d79bb6dbb7f906d8825223acf98c67eba7556638))
* **warnings:** fix server response differs from the user-provided base URL ([0f1afdb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0f1afdb620c4283644ef43d2f8a532a94041455f))
* **warnings:** replace assertRegexpMatches by assertRegex ([d410744](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d410744d548545671b090dfff881c32b8c7fd96f))


### :repeat: Chore

* **ci:** run commitlint job sooner ([e3b919f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e3b919fa8aa50b6293cc4e951b5eeb0e0f16fc51))
* **deps:** update dependency flake8-bugbear to v22.9.23 ([969f689](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/969f6897d96584094a628a9d78e97097ef8ba954))
* **deps:** update dependency flake8-bugbear to v22.9.23 ([62f59f9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/62f59f9d98f4700add691603ae3bf177184a8b36))
* **deps:** update dependency types-html5lib to v1.1.11 ([d3e220c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d3e220cdf2155ad05c7b3ab2d1d7661ae42bc0d4))
* **deps:** update dependency types-html5lib to v1.1.11 ([bfcb786](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bfcb786f505cc360f0bbf558c6218b2442220bac))
* **deps:** update node.js to v18.9.1 ([64450ae](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/64450ae1f90c23637f33af6ccec05c3886fa3581))
* **deps:** update node.js to v18.9.1 ([8d9fc8c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8d9fc8c1f6e4a114ef6a96c986aad5dd367f9103))


### :memo: Documentation

* update release docs ([dc08c5a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dc08c5a32220a823ce0344bf1cfeab51b8b359bb))

## [1.9.1](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.9.0...v1.9.1) (2022-09-20)


### :repeat: Chore

* **ci:** expire artifacts in 8 weeks ([f17b1fe](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f17b1fea928992dec47c40b1f8d8d4601af3af85))
* **deps:** update dependency flake8-bugbear to v22.9.11 ([9f7a880](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9f7a8804ed8d8e8bacea389e1737dd76bdc0a90b))
* **deps:** update dependency flake8-bugbear to v22.9.11 ([96beccd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/96beccdee17b69639d3da7d6c635c1397d6c6834))
* **deps:** update dependency jsonschema to v4.15.0 ([55b234a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/55b234afdd2b714f4422321ab5e9eb2ecf0cced3))
* **deps:** update dependency jsonschema to v4.15.0 ([89596b5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/89596b578ad9e5183f7dc8260ffd0c9ba81057fb))
* **deps:** update dependency jsonschema to v4.16.0 ([b24407b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b24407b44e158a90b31ed4566307b53ccbe59baa))
* **deps:** update dependency jsonschema to v4.16.0 ([1f0047d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1f0047d45530c03c858077a5a6407f772438107f))
* **deps:** update dependency pylint to v2.15.2 ([618873d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/618873d89bc15cd886da7859a2525c651a1f4c6a))
* **deps:** update dependency pylint to v2.15.2 ([0b46ac7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0b46ac7edb53b5d0b0acc2973dd4817b3e4ccca4))
* **deps:** update dependency pylint to v2.15.3 ([6cbda9b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6cbda9be4a4eb1087224bdb77b764beab04fa2fb))
* **deps:** update dependency pylint to v2.15.3 ([853115d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/853115da5b05e9e94428018e56096bf087a13e0c))
* **deps:** update dependency types-requests to v2.28.10 ([ee387c5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ee387c56c64341fdfad1444c720600d0782e39db))
* **deps:** update dependency types-requests to v2.28.10 ([04dba69](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/04dba69c6203d56398ce02b196e526b5f433cba5))
* **deps:** update dependency vcrpy to v4.2.1 ([5462f41](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5462f41a37e4668602301460896187636b3fdc82))
* **deps:** update dependency vcrpy to v4.2.1 ([d278065](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d278065a7a1f54b47a738613a86d4a9907062b97))
* **deps:** update node.js to v18.9.0 ([639044f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/639044feeaade7ed7de88ec9683abc54f77ec83f))
* **deps:** update node.js to v18.9.0 ([323f2b7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/323f2b7037475a5e47519a85cbb892c340c3e72a))


### :bug: Fixes

* **tests:** skip application settings tests with python-gitlab < 3.7 ([d0a0c15](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d0a0c15b5b0b248edd8dce30e9507f5e252c083f))
* **tests:** skip application settings tests with python-gitlab < 3.7 ([0b6a0e8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0b6a0e8d2dcc0c4bcb5ce70859ddf24ae95c4844)), closes [#1020126](https://gitlab.com/gitlabracadabra/gitlabracadabra/issues/1020126)

## [1.9.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.8.1...v1.9.0) (2022-08-31)


### :sparkles: Features

* support all possible state transistions ([8897fdc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8897fdc813c716925629921165ead30782106801))
* support all possible state transistions ([bd91f70](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bd91f7078a6334a8d8682cfd65b0c3d611240f04))


### :bug: Fixes

* **helm:** issue a warning instead of an info when package is not found ([708fb4a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/708fb4aab32bd29ebd1ccacd9b79ecfebd2599fa))
* **helm:** issue a warning instead of an info when package is not found ([0593ca2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0593ca21adfbf1e9364e086a84c6b76fee625dd9))
* **image_mirrors:** set user-agent ([678216e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/678216e2290fab1c37bc8e9f5338869d00d32deb))
* **image_mirrors:** set user-agent ([6486cdc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6486cdcc4fdea8c06d1615fed55c41d2bcbed83a))


### :repeat: Chore

* **deps:** remove explicit dependency on pep8-naming ([6c7ec1d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6c7ec1d559fac9e8a2f9300a6f8add575f0ba726))
* **deps:** update dependency coverage to v6.4.2 ([a67166a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a67166ac025bbe9b094bd78efc05f8ca317a79e9))
* **deps:** update dependency coverage to v6.4.2 ([fd04df9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fd04df98e055179c7aabfa94c61af3b644e78156))
* **deps:** update dependency coverage to v6.4.3 ([c1b1e6a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c1b1e6ad140b76410687a816cbc291c397dc33b0))
* **deps:** update dependency coverage to v6.4.3 ([59728b6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/59728b6f1c00213690c2f7b4111c970b92cff554))
* **deps:** update dependency coverage to v6.4.4 ([cbe7b81](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cbe7b8158dbfbbeb928704ea705b16a5d717e240))
* **deps:** update dependency coverage to v6.4.4 ([2749ac6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2749ac69775b770ae8692c45c1af7d29470d791f))
* **deps:** update dependency flake8-bugbear to v22.7.1 ([dc811db](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dc811dbbfa10af639bd7fe2f43527bd497702b28))
* **deps:** update dependency flake8-bugbear to v22.7.1 ([61304a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/61304a32b0a9f8ce8c2cbe0ca618b410dad5292b))
* **deps:** update dependency flake8-bugbear to v22.8.22 ([38956fc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/38956fce165ad1c65c5d0465a9edaa6acc8d1450))
* **deps:** update dependency flake8-bugbear to v22.8.22 ([92005ec](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/92005ec9043e475bcbf6e828ca5720f237084138))
* **deps:** update dependency flake8-bugbear to v22.8.23 ([6206d55](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6206d559d49da96cb2fae78cd64e6e320e87d2c7))
* **deps:** update dependency flake8-bugbear to v22.8.23 ([3e50261](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3e5026166b262adc0fd61c135bdaf9f76acc556c))
* **deps:** update dependency flake8-logging-format to v0.7.4 ([c58c205](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c58c205447ea5bf94cfc38f06b6a9064055b3241))
* **deps:** update dependency flake8-logging-format to v0.7.4 ([fcc9f7b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fcc9f7bc170def86176fc40558c608f40c20d129))
* **deps:** update dependency flake8-logging-format to v0.7.5 ([4cbba72](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4cbba7293347ebdcc6614b28f311089e072690fb))
* **deps:** update dependency flake8-logging-format to v0.7.5 ([8989949](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/89899497311c47b30b07f63cfa6db8a99c35af84))
* **deps:** update dependency flake8-rst-docstrings to v0.2.7 ([838416b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/838416bf394eb09b27244225042bac03c21fcbc2))
* **deps:** update dependency flake8-rst-docstrings to v0.2.7 ([9713083](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9713083c593f03d63425db79bb8294d540c165e6))
* **deps:** update dependency jsonschema to v4.14.0 ([69fcc8d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/69fcc8dbe6a12825d4d3a2262a9b2d25a80aba8c))
* **deps:** update dependency jsonschema to v4.14.0 ([7a1b811](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7a1b8119431dd8625a7872af32ad93841028f84a))
* **deps:** update dependency jsonschema to v4.6.2 ([d76c2de](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d76c2dea4d6f65cfb1df8ee14828b696e1852d4a))
* **deps:** update dependency jsonschema to v4.6.2 ([d64eb4c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d64eb4c1c6ea7ce972e6ad260427a0d4738fd257))
* **deps:** update dependency jsonschema to v4.7.2 ([9804e2e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9804e2e1247cee92dcf52cb5b32cb072bea3ec98))
* **deps:** update dependency jsonschema to v4.7.2 ([338e85f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/338e85f447f62519509fac48bc679a4b0f64187b))
* **deps:** update dependency jsonschema to v4.9.0 ([98e6456](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/98e6456fcf597f22c16c57a489259a0fbe4e1d81))
* **deps:** update dependency jsonschema to v4.9.0 ([9a0d698](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9a0d698ee86633fe1b9e9869ce177c627c560216))
* **deps:** update dependency jsonschema to v4.9.1 ([18fb8a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/18fb8a32e91746e68f832bd9fb5a4518b6f501c7))
* **deps:** update dependency jsonschema to v4.9.1 ([09adad9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/09adad9a2a290ca99a296fa77e3bdad5f80f7e03))
* **deps:** update dependency mypy to v0.971 ([2338efa](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2338efa4273c7bc6a54e26622e70490d21faab3f))
* **deps:** update dependency mypy to v0.971 ([49e69dd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/49e69dd0b83a9a679ea7d43e1ec7164f564a985a))
* **deps:** update dependency pep8-naming to v0.13.0 ([e53c0c0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e53c0c047d6aa6d01a3db27c46c9ea634745d2d4))
* **deps:** update dependency pep8-naming to v0.13.0 ([c068754](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c0687545d2c461cca9574bb5387b9ee52df81e32))
* **deps:** update dependency pylint to v2.14.4 ([3c97aa8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3c97aa89689d4a279e129e5a2d507bf385b2984e))
* **deps:** update dependency pylint to v2.14.4 ([d6b1b7c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d6b1b7c3a48e5ee784b2a23cc7fb8431fe498af9))
* **deps:** update dependency pylint to v2.14.5 ([eef1c54](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eef1c5419dc9f05227a82a6d46c36cc68071aa0d))
* **deps:** update dependency pylint to v2.14.5 ([224cbc5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/224cbc502cfcb0231cda93713ddd371a71f805b5))
* **deps:** update dependency pylint to v2.15.0 ([a39d325](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a39d325c2faf80f589c96754282ae259e1b89af8))
* **deps:** update dependency pylint to v2.15.0 ([2dc1f11](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2dc1f111e7e3da69550c4bd9ef6e58b886ce5e02))
* **deps:** update dependency python-gitlab to v3.9.0 ([fdeffdb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fdeffdbe8471e204f9117db6145e825c8dee41b3))
* **deps:** update dependency python-gitlab to v3.9.0 ([38fc6a4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/38fc6a45ab9e5bd85f20d211b8f2b062000e1a8c))
* **deps:** update dependency types-html5lib to v1.1.10 ([39f0523](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/39f05236a7c2c5c5e95cec7f6ba70ec3635eddb2))
* **deps:** update dependency types-html5lib to v1.1.10 ([a391431](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a3914311e6294319cd0baac563192dd21337d1fe))
* **deps:** update dependency types-html5lib to v1.1.8 ([a7003e9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a7003e9a1b4133c3fde1c1689e10cc0ed5810bcd))
* **deps:** update dependency types-html5lib to v1.1.8 ([471c50a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/471c50a1908c9d22f1f4703d37588ba6292e2139))
* **deps:** update dependency types-html5lib to v1.1.9 ([9f3e94a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9f3e94a65f4f26d72ccae3dd347dbe8e7c8b794c))
* **deps:** update dependency types-html5lib to v1.1.9 ([4df7d27](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4df7d27348b28c745856abec6ccfbcbb9bbe48cc))
* **deps:** update dependency types-pyyaml to v6.0.10 ([83f8ba1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/83f8ba17d9904dae99c251e73729d6692948d7cb))
* **deps:** update dependency types-pyyaml to v6.0.10 ([6204c9a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6204c9a5903b2a4e405a042480e6c26de2e33d89))
* **deps:** update dependency types-pyyaml to v6.0.11 ([f6604f5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f6604f5d43807d0286c48d4e78d6b6277d32cd31))
* **deps:** update dependency types-pyyaml to v6.0.11 ([48d0159](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/48d01597fa68048d9e1e4b4dcc141daedbc142dd))
* **deps:** update dependency types-requests to v2.28.2 ([d95e8f9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d95e8f92813c3d7ed0b2d6afc7904a139bd3d035))
* **deps:** update dependency types-requests to v2.28.2 ([c693e6d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c693e6d40d4fb48c4e225f7047ebed9eefcbfb9e))
* **deps:** update dependency types-requests to v2.28.3 ([c5d2185](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c5d21857d69c04e18ba70b9b4b5368292c81e5ab))
* **deps:** update dependency types-requests to v2.28.3 ([f288bc3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f288bc3d230ab83e1d05637a06ef231b9846770c))
* **deps:** update dependency types-requests to v2.28.7 ([9a8cddd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9a8cddd9bfd1ecea26b47c202b9562497e6bc3c7))
* **deps:** update dependency types-requests to v2.28.7 ([2765582](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2765582042dbdf6911f83542bc0b75b72d998ab5))
* **deps:** update dependency types-requests to v2.28.8 ([0c376dd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0c376dd0b02769f6235b07d938674f0f73d99a89))
* **deps:** update dependency types-requests to v2.28.8 ([b5cead2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b5cead28d5568e4a69058ea5436e663be1ccc45f))
* **deps:** update dependency types-requests to v2.28.9 ([309e660](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/309e6607a53b75df646a34d280696468cb76fb76))
* **deps:** update dependency types-requests to v2.28.9 ([0ede4e0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0ede4e025d49ef3b21e2305829e587a2c65da091))
* **deps:** update dependency vcrpy to v4.2.0 ([f284e04](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f284e040501dfd6e73331615c8af025b078cad4d))
* **deps:** update dependency vcrpy to v4.2.0 ([d63bb81](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d63bb8162db61f55f67cd81f47efe1b86d24c34f))
* **deps:** update node.js to v18.6.0 ([c681fd6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c681fd6eead254a3c08261f71b4ee008a1df3dfb))
* **deps:** update node.js to v18.6.0 ([617b78a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/617b78a858db151ce4df25fef058a939f8db21a6))
* **deps:** update node.js to v18.7.0 ([3e11ee8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3e11ee84b5fc72fd641f257c9365c33b7a33519e))
* **deps:** update node.js to v18.7.0 ([9c0347f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9c0347f1b3b2b1a4512ad6253e1435933319fcca))
* **deps:** update node.js to v18.8.0 ([6ad1340](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6ad134017e7ac95e0a39a1be2097ffc0cddb7a47))
* **deps:** update node.js to v18.8.0 ([8136197](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/81361973f1019c3fcbfe505b87a9e8cb38eda861))
* **deps:** update tests for python-gitlab >= 3.7 ([81449a1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/81449a187aa6effa8f98d8b32b6a5821626b737f))

## [1.8.1](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.8.0...v1.8.1) (2022-06-29)


### :memo: Documentation

* update release instructions ([eadf5aa](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eadf5aaa9fecfe3659385bb803ea0beff3f5a48f))


### :repeat: Chore

* **debian:** add gbp.conf for default branch ([07cb354](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/07cb354c21122c459a9c770f228f2fdf92e1a6ca))
* **debian:** update tar-ignore list ([fda96c6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fda96c6d82cbdc9e316d28c889669807d738878d))
* **release:** add changelog title ([195aa95](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/195aa9515ad58e2da33e4718dc109d04be2ef941))


### :question: Unclassified

* Release debian ([6fd1749](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6fd1749fca738d7fc66793e6c59d8452d3b88915))

## [1.8.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.7.0...v1.8.0) (2022-06-29)


### :question: Unclassified

* Development mode for 1.8.0 ([0e48396](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0e48396c3a38f21d16e55445bc5a6919db4ce74d))
* Development mode for 1.8.0 ([baa3408](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/baa34087bbe25367462fc9a4a3b408e796f3cfe9))


### :memo: Documentation

* development environment ([985d086](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/985d08689dff3d7f3d633e61c7614edc3beb8bf6))
* development environment ([2cf3fe0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2cf3fe0729fff77ea845d47da8be01c3f2e8dc00))
* improve contribution docs and add commitlint ([c8c53a6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c8c53a669ea9abfe4e9a303f96bd23ad97474262))
* improve contribution docs and add commitlint ([fb7ec97](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fb7ec97dea4582ea5840b5c0417786ea45c00c75))


### :sparkles: Features

* **registry:** fix auth with redirect ([b1cd348](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b1cd3482bf9583c5db863c359e12cafcdb7119bf))
* **registry:** fix auth with redirect ([47a87f9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/47a87f9ffdf794da75bd50c49cc6f95dff240868))
* rename default branch from master to main ([70941c2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/70941c2232f1278ade1e97ff0b0a37fae633f69e))


### :bug: Fixes

* update gitlabracadabra.yml to current state ([e2e128d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e2e128d28751d11df190ac9e80c0d103faf4e619))


### :repeat: Chore

* **deps:** update dependency coverage to v6.3.3 ([da8e632](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/da8e63259d3c152a67d4823e07a815f24f246d2f))
* **deps:** update dependency coverage to v6.3.3 ([72eb633](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/72eb633fd6b425ecc06e271d16b3eec6a2707054))
* **deps:** update dependency coverage to v6.4 ([a6bf80a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a6bf80a39f1228c7cc46dbda78419f24fdea0ec2))
* **deps:** update dependency coverage to v6.4 ([3f98ae0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3f98ae0d15d021a1418283a335c2cdf5a7216cae))
* **deps:** update dependency coverage to v6.4.1 ([3b38e94](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3b38e947b2c3d2ee71563a01b6843e378a86db6f))
* **deps:** update dependency coverage to v6.4.1 ([12f64fc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/12f64fc5e113a07326f564777491ebe92a9038c6))
* **deps:** update dependency flake8-bugbear to v22.6.22 ([351f068](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/351f0688b72e60b68bb5f17eec3b70b9e700c933))
* **deps:** update dependency flake8-bugbear to v22.6.22 ([ac9fd91](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ac9fd9166877b92a5839d06484ac8deb222d4a67))
* **deps:** update dependency flake8-comprehensions to v3.10.0 ([73789ca](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/73789cafeb185c86dc4bd7958acc2fe09d11f4a3))
* **deps:** update dependency flake8-comprehensions to v3.10.0 ([4bc5d6c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4bc5d6ce5d0756b21f17cd778f0e7d6537d8be31))
* **deps:** update dependency flake8-comprehensions to v3.9.0 ([eadbb41](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eadbb41adcb52feabb199821f82ad22b1726756b))
* **deps:** update dependency flake8-comprehensions to v3.9.0 ([641e012](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/641e0127390d413d5ac9dc9d0955b63fb4839201))
* **deps:** update dependency flake8-rst-docstrings to v0.2.6 ([4d97c51](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4d97c5121aee1a8916b63721821346c54b278f94))
* **deps:** update dependency flake8-rst-docstrings to v0.2.6 ([07775af](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/07775af01a38a6e29dc15f58f2d6efbe2afd8c03))
* **deps:** update dependency flake8-tidy-imports to v4.7.0 ([8c7ef60](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8c7ef6084c9a6a6c3dc82c4138d1fa0f3ea17bef))
* **deps:** update dependency flake8-tidy-imports to v4.7.0 ([a770e0c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a770e0c15d9aa8dd0bdc41784182a0f30053e06d))
* **deps:** update dependency flake8-tidy-imports to v4.8.0 ([0de2c83](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0de2c833cd498bbec8dbaf1f2fb98d8d04d1dfb8))
* **deps:** update dependency flake8-tidy-imports to v4.8.0 ([e2ebf16](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e2ebf1613f6dd0d163187d8577041f5116207a8c))
* **deps:** update dependency jsonschema to v4.6.0 ([a01b161](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a01b16135585a843b2d3ff3ae88e8e479c4fe255))
* **deps:** update dependency jsonschema to v4.6.0 ([cfec8c6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cfec8c663099aedb0d86ef38cfdcffc60e7d40b1))
* **deps:** update dependency jsonschema to v4.6.1 ([a554e98](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a554e98c4d503df1e42b2e45fdeaa6bbfe6d0224))
* **deps:** update dependency jsonschema to v4.6.1 ([b4efe38](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b4efe38cbb5bd24c1ec67727c22dff94b08dc19b))
* **deps:** update dependency mypy to v0.960 ([f605cad](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f605cadcce6313354096d7a321f59126f2e032d8))
* **deps:** update dependency mypy to v0.960 ([59e8601](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/59e86015f4fbfc0332251af99fd66497612525e0))
* **deps:** update dependency mypy to v0.961 ([0afa959](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0afa9594d62211c34883c0e4a04d101460dbf8ea))
* **deps:** update dependency mypy to v0.961 ([c519925](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c5199250ed55092446cfef6538f6e40f2b22326e))
* **deps:** update dependency pylint to v2.13.9 ([72b40ea](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/72b40ea7223d92ba13cc59b823957a932c0dd120))
* **deps:** update dependency pylint to v2.13.9 ([b0fdc04](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b0fdc041e79b0d7167a911262e91c60fca13872b))
* **deps:** update dependency pylint to v2.14.0 ([3232235](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3232235ab15f48268c57169567d07b4e172e7e97))
* **deps:** update dependency pylint to v2.14.0 ([e18cd1b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e18cd1bb7326c712e7502a6aea182a64faac24f7))
* **deps:** update dependency pylint to v2.14.1 ([0e345a7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0e345a7e05b3e9457b40c6ea232aa98ab967b43d))
* **deps:** update dependency pylint to v2.14.1 ([38b0b92](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/38b0b9226e611fc691091b3e268da0b1ce1a7556))
* **deps:** update dependency pylint to v2.14.2 ([3f815f0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3f815f05744702c58273c59b3badcf36ced50228))
* **deps:** update dependency pylint to v2.14.2 ([ff03c77](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ff03c77fd819e77cb60cd102027c43c2fa9851c2))
* **deps:** update dependency pylint to v2.14.3 ([1862cf6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1862cf6f499fadf64646272a44ca17853f5ee4d0))
* **deps:** update dependency pylint to v2.14.3 ([5901c41](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5901c41a8acebef123f9594df8ee8fa3a312921b))
* **deps:** update dependency python-gitlab to v3.5.0 ([542dc4a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/542dc4aadb8855ce53f39f8845f8cec0e4851b46))
* **deps:** update dependency python-gitlab to v3.5.0 ([6a81e69](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6a81e69452d609a0fe96bba4ab0c6aecce704eff))
* **deps:** update dependency python-gitlab to v3.6.0 ([e8616bd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e8616bd8cde5bf8d68ebbd14fce743550b8a5d5e))
* **deps:** update dependency python-gitlab to v3.6.0 ([7277087](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7277087aca035323143dfb1d70dbde06fc0f4383))
* **deps:** update dependency semantic_version to v2.10.0 ([a881708](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a88170885d93fdac7dbb794e5da159527bdf332b))
* **deps:** update dependency semantic_version to v2.10.0 ([f78d7e6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f78d7e69dcf7915dc8326c28bd904ea1fd975606))
* **deps:** update dependency types-pyyaml to v6.0.8 ([ed6553f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ed6553f46a42a342b6590f1a3e402f78a9e53913))
* **deps:** update dependency types-pyyaml to v6.0.8 ([9071af1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9071af138874b592201cd98debab91fff9a4a943))
* **deps:** update dependency types-pyyaml to v6.0.9 ([049a4da](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/049a4da8737013202f5896c5abfd6f859678db95))
* **deps:** update dependency types-pyyaml to v6.0.9 ([bf35536](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bf35536a1245eeefb439ad1d9f65925897802525))
* **deps:** update dependency types-requests to v2.27.26 ([e224467](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e224467b2c21fd01f51dc1c37467cda11c7358c2))
* **deps:** update dependency types-requests to v2.27.26 ([eaad137](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eaad137de33f457ad42e83dd4a7705ef6d9f789c))
* **deps:** update dependency types-requests to v2.27.27 ([2793647](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/27936475b5dd542287a5c2cbe20534732e1a34a1))
* **deps:** update dependency types-requests to v2.27.27 ([05820b4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/05820b4e2cbeff886d11a1e82fe1690c9b035ef3))
* **deps:** update dependency types-requests to v2.27.28 ([da01e67](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/da01e671b06ba1587f476128a67c65a20929fa1f))
* **deps:** update dependency types-requests to v2.27.28 ([61bf5c1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/61bf5c130b50ee808afaed41ad861c06b8d15459))
* **deps:** update dependency types-requests to v2.27.29 ([593ea63](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/593ea6374e4cd11a56dd36be85b33345fa4bd84e))
* **deps:** update dependency types-requests to v2.27.29 ([e003ace](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e003acebbdad234bca3e6f2b476ebca3f6b850ee))
* **deps:** update dependency types-requests to v2.27.30 ([050a457](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/050a457e5d888a12e13e61456c3eb9cde5b67a05))
* **deps:** update dependency types-requests to v2.27.30 ([9703c32](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9703c3258950bac6aea1417751ee4c0f8dd239a3))
* **deps:** update dependency types-requests to v2.27.31 ([62cd83f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/62cd83fdf9b9804036e29415f1f2726c1d617d80))
* **deps:** update dependency types-requests to v2.27.31 ([2a80208](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2a802088650d5ababf2a61d9619e733bbcd14abf))
* **deps:** update dependency types-requests to v2.28.0 ([b9b67a9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b9b67a99e3607e783a191ec7c5b2cb61e4700b51))
* **deps:** update dependency types-requests to v2.28.0 ([90e3731](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/90e373103b32f174ccbdff81f30980eb10e91c2b))
* future releases with semantic-release ([afd1bd7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/afd1bd7d678e0168179ba377e82cea4a6bd70e74))
* future releases with semantic-release, and rename default branch from master to main ([c78dae2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c78dae2e61d679b404ef055b98072b4f71a3aaac))
* **parser:** fix C417  Unnecessary use of map ([ef10d19](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ef10d190efdadd06848a8496d292ccac0559e3a7))

## 1.7.0 (2022-05-06)

**Enhancements:**

- Use Version.coerce instead of the strict version

**Tooling:**

- From `renovate`:
  - chore(deps): update dependency types-requests to v2.27.22
  - chore(deps): update dependency mypy to v0.950
  - chore(deps): update dependency python-gitlab to v3.4.0
  - chore(deps): update dependency types-requests to v2.27.24
  - chore(deps): update dependency types-requests to v2.27.25
  - chore(deps): update dependency flake8-debugger to v4.1.2
  - chore(deps): update dependency pylint to v2.13.8
  - chore(deps): update dependency jsonschema to v4.5.1## 1.6.0 (2022-04-26)

## 1.6.0 (2022-04-26)

**Enhancements:**

- urllib3: Lower retry message from WARNING to INFO (!295)

**Tooling:**

- tests: Use temp config instead of mocking (!303)
- From `renovate`:
  - chore(deps): update dependency types-requests to v2.27.19 (!298)
  - chore(deps): update dependency types-html5lib to v1.1.7 (!296)
  - chore(deps): update dependency types-pyyaml to v6.0.6 (!297)
  - chore(deps): update dependency pylint to v2.13.7 (!299)
  - chore(deps): update dependency types-pyyaml to v6.0.7 (!300)
  - chore(deps): update dependency types-requests to v2.27.20 (!301)
  - chore(deps): update dependency flake8-bugbear to v22.4.25 (!302)

## 1.5.0 (2022-04-13)

**Enhancements:**

- retry package connections 3 times (!265)
- Support regexp for Helm package name (!266)
- registry importer: Warn for all HTTP errors (#46, !267)
- python-gitlab v3 compatibility (!240, Debian's [#1009428](https://bugs.debian.org/1009428))

**Tooling:**

- Pin bandit to 1.7.2 (!268)
- Fix A504  prefer the 'msg=' kwarg for assertTrue() diagnostics (!276)
- python-gitlab v3 compatibility (!240)
  - Add TestCase.gitlab_version() to compare pygitlab version
  - python-gitlab v3: config handling has changed again
  - python-gitlab v3: Some urls changed
  - Update application settings schema
  - Always change values when processing a dependency
  - Update Application settings tests for pygitlab v3
- From `renovate`:
  - chore(deps): update dependency types-requests to v2.27.9 (!261)
  - chore(deps): update dependency types-requests to v2.27.10 (!262)
  - chore(deps): update dependency coverage to v6.3.2 (!263)
  - chore(deps): update dependency types-requests to v2.27.11 (!264)
  - Update dependency types-requests to v2.27.12 (!272)
  - Update dependency wemake-python-styleguide to v0.16.1 (!273)
  - Update dependency mypy to v0.940 (!274)
  - Update dependency mypy to v0.941 (!275)
  - Update dependency types-PyYAML to v6.0.5 (!277)
  - Update dependency flake8-bugbear to v22 (!247)
  - Update dependency types-requests to v2.27.13 (!278)
  - Update dependency flake8-assertive to v2.1.0 (!276)
  - Update dependency bandit to v1.7.4 (!279)
  - Update dependency flake8-bandit to v3 (!270, !279)
  - Update dependency flake8-blind-except to v0.2.1 (!280)
  - Update dependency types-requests to v2.27.14 (!281)
  - Update dependency flake8-bugbear to v22.3.20 (!282)
  - Update dependency flake8-bugbear to v22.3.23 (!283)
  - Update dependency mypy to v0.942 (!284)
  - Update dependency pylint to v2.13.0 (!285)
  - Update dependency pylint to v2.13.2 (!286)
  - Update dependency types-requests to v2.27.15 (!287)
  - Update dependency pylint to v2.13.3 (!288)
  - Update dependency pylint to v2.13.4 (!289)
  - Update dependency types-requests to v2.27.16 (!290)
  - Update dependency types-html5lib to v1.1.6 (!291)
  - Update dependency pylint to v2.13.5 (!292)
  - chore(deps): update dependency python-gitlab to v3 (!240)

## 1.4.0 (2022-02-08)

**Enhancements:**

- pypi: Always sync yanked packages when using equal specifier (!229)
- Fix Python GitLab link in doc (!215, #44)
- Update gitlabracadabra.yml with latest parameters (!222)
- retry registry connections 3 times (!257)

**Tooling:**

- Fix WPS324 Found inconsistent `return` statement (!233)
- Fix isort (!213)
- renovate: Remove hourly rate limit for PR creation (!234)
- From `lintian-brush`:
  - Bump debhelper from old 12 to 13 (!208)
  - debian/rules: Drop --fail-missing argument to dh_missing, which is now the default (!208)
  - Update standards version to 4.6.0, no changes needed (!208)
  - Remove constraints unnecessary since buster (!256)
- From `renovate`:
  - Update dependency coverage to v6.1.2 (!209)
  - Update dependency types-PyYAML to v6.0.1 (!210)
  - Update dependency PyYAML to v5.4.1 (!211)
  - Update dependency packaging to v21.3 (!212)
  - Update dependency wemake-python-styleguide to v0.15.3 (!213)
  - Update dependency PyYAML to v6 (!214)
  - Pin flake8-broken-line==0.3.0 (!213)
  - Pin flake8 (!213)
  - Pin pycodestyle (!213)
  - Pin pep8-naming (!213)
  - Update dependency types-requests to v2.26.1 (!216)
  - Update dependency coverage to v6.2 (!217)
  - Update dependency flake8-bugbear to v21.11.29 (!219)
  - Update dependency flake8-rst-docstrings to v0.2.5 (!220)
  - Update dependency types-html5lib to v1.1.3 (!223)
  - Update dependency types-requests to v2.26.2 (!224)
  - Update dependency jsonschema to v4.3.2 (!225)
  - Update dependency mypy to v0.930 (!226)
  - Update dependency jsonschema to v4.3.3 (!227)
  - Update dependency types-html5lib to v1.1.4 (!228)
  - chore(deps): update dependency pylint to v2.12.2 (!231)
  - chore(deps): update dependency types-requests to v2.27.0 (!232)
  - chore(deps): update dependency wemake-python-styleguide to v0.16.0 (!233)
  - chore(deps): update dependency flake8-broken-line to v0.4.0 (!218)
  - chore(deps): update dependency pep8-naming to v0.12.1 (!221)
  - chore(deps): update dependency types-html5lib to v1.1.5 (!235)
  - chore(deps): update dependency types-requests to v2.27.1 (!237)
  - chore(deps): update dependency flake8-assertive to v2 (!239)
  - chore(deps): update dependency types-pyyaml to v6.0.2 (!236)
  - chore(deps): update dependency flake8 to v4 (!238)
  - chore(deps): update dependency pycodestyle to v2.8.0 (!238 superseding !230)
  - chore(deps): update dependency types-requests to v2.27.5 (!243)
  - chore(deps): update dependency mypy to v0.931 (!244)
  - chore(deps): update dependency types-pyyaml to v6.0.3 (!242)
  - chore(deps): update dependency flake8-comprehensions to v3.8.0 (!245)
  - chore(deps): update dependency flake8-tidy-imports to v4.6.0 (!246)
  - chore(deps): update dependency types-requests to v2.27.6 (!248)
  - chore(deps): update dependency jsonschema to v4.4.0 (!249)
  - chore(deps): update dependency types-requests to v2.27.7 (!250)
  - chore(deps): update dependency coverage to v6.3 (!251)
  - chore(deps): update dependency types-pyyaml to v6.0.4 (!253)
  - chore(deps): update dependency types-requests to v2.27.8 (!254)
  - chore(deps): update dependency coverage to v6.3.1 (!255)
  - chore(deps): update dependency semantic_version to v2.9.0 (!258)

## 1.3.0 (2021-11-10)

**New features:**

- Mirror PyPI packages (!183, !184, !185, !192, !193)
- Add squash_option support (!189, !198)
- Add user state support (!197, #41)

**Fixes:**

- package mirrors:
  - Keep original request URL, to avoid too long filename (!186)
  - Consider 202 Accepted as upload success (!190)
  - Use default Accept-Encoding (!196)
- image mirrors:
  - Force syncing all platforms when digest is set (!194)
  - Do not register blobs from manifest lists (!195)
  - Catch manifest import errors (!200, #42)

**Tooling:**

- Do not build Universal Wheel anymore (!182)
- Fix pep8 since wemake update (!199)
- Add renovate config (!201)
  - Pin dependencies (!202)
  - Update dependency isort to v5 (!203)
  - Pin all python packages (!204)
  - Pin mypy dependencies (!204)
  - Pin CI images (!205)
- Build deb on bullseye (!197)

## 1.2.0 (2021-06-20)

**New features:**

- Helm chart repository support (!169, !170, !171, #38, #39, !174, !179)
  - With repo_url, package_name, versions, semver, limit and channel parameters

**Enhancements:**

- warn instead of info for not found and other non-ok status (!172)
- CI:
  - Fix mock_calls on buster (!173)
  - Fix ProxyError message on buster (!173)
  - Fix buster linting issue (!179)
  - Update to latest mypy and types-* packages (!176)
  - Fix mypy errors (!177)
  - PyGitlab now converts arrays to comma-separated list as string (!177)
  - Reduce noqa usage (!178)

**Fixes:**

- Fix format order in package file upload logging (!172)
- Warn on HTTP errors instead of failing (#39, !172)
- Fix github assets upload (!175)
- Ensure parameter is mangled on change. Fixes: first_day_of_week in
  application settings (!177)

## 1.1.0 (2021-05-21)

**New features:**

- :rocket: Package mirroring (#31):
  - Raw URLs to generic packages (!156)
  - Github releases to generic packages
    (#34, #35, #37, !159, !160, !161, !162, !163, !164, !165)

**Enhancements:**

- Improve JSONschema errors
- Image mirrors features:
  - Store Blob sizes to avoid uneeded HEAD request when printing stats (!153)
  - Better manifest v1 handling: Avoid 404 errors when printing stat and with
    mounted blobs (#33, !153, !154, !155)
- Misc:
  - Fix hadolint: Multiple consecutive `RUN` instructions (!153)
  - Update requirements for libgit2 1.0 transition (!156)

## 1.0.0 (2021-04-17)

**Important fixes:**

- **Security**: Ensure TLS certificates is verified with container registries (!139)

**Enhancements:**

- Image mirrors features:
  - Implement SemVer (!148)
  - Support manifest v1 (#28, !143, !145)
  - Add OCI Image Media Types (!149)
  - Allow to synchronize image by digest (!138)
  - Handle registries without Docker-Content-Digest header (!141)
  - Avoid checking signed manifest digest (#29, !146)
  - Increase the chunk size to 50MB (!142)
  - Improve heuristics:
    - Always GET manifests as they are small (!146)
    - Register seen blobs from manifests (!146)
    - Check if blob exists before uploading (!146)
    - Avoid unneeded HTTP query just to get size (!146)
  - Allow tag regexp for string-form from too (!147)
- Lower NOT Deleting %s (not found) from INFO to DEBUG (!144)

**Fixes:**

- Image mirrors fixes:
  - Avoid infinite stack trace with cache_path (!140)
  - Fix blob mount (#30, !146)
  - Handle token expiration (!150)
- Use hadolint/hadolint:latest-alpine for hadolint job (!138)

## 0.9.1 (2021-03-20)

**Enhancements:**

- Sync debian/source/options tar-ignore with .gitignore (!134)
- Add dep5 tests (autopkgtest) (!135)

## 0.9.0 (2021-03-19)

**New features:**

- :rocket: Docker image mirroring (#13):
  - Basic mirroring (!114, !117, !118, !119 , !120, !123)
  - Support selecting images by base+repositories+tags (!128)
  - Tag matching with regexps (!129)
  - Bug fixes (#27, !127)
- Support for multiple GitLab connections, including `mirrors` (!130, !131)

**Enhancements:**

- Use shorter default logging format (!125)
- Improve error when `extends:` is not found (!101)
- mirrors: Enable automatic proxy detection when available (#25, !115, !125)
- User: Add skip_reconfirmation, note (!116)
- Docker image:
  - Use Debian package on Docker image (!102)
    - Removes pip usage
    - Ensure certificate usage is centralized (no `/usr/local/lib/python3.7/dist-packages/certifi/cacert.pem`)
    - Use `bullseye`, as `buster` packages are older
  - Test the Docker image (!103)
- Documentation:
  - Add the missing `v` prefix in Docker image tag (!99)
  - Fix docker command, and mention other images (!105)
  - Document ability to mirror Git repositories (!108)
- Code cleanup and developpement tooling:
  - Improve pep8 (!98, !109, !110), !111
  - Use "build" job from AutoDevOps (!104)
  - Refactor test utils (!106)
  - Drop python2 support (!110)
  - Initial mypy checks (!114)

**Fixes:**

- Always remove trailing slash, even when type is specified (#23, !100)
- Fix: is_admin -> admin param for creation or update (#26, !112, !122)
- Create cache dir (#19, !121, !124)

## 0.8.0 (2021-02-09)

Thanks to Sébastien Heurtematte who contributed two merge requests (!91, !93)!

**New features:**

- Mirror branch mapping (!83)
- Project and Group Boards (!85, !86)
- Add support for Docker retention policy (!84)
- Add rename_branches parameter (!89)

**Enhancements:**

- Various pep8 improvements and added flake8 plugins (!87, !88)
- Enhance docker build and debug (!93)
- Improve CI (!95)
- Documentation:
  - Document "Using gitlabracadabra in GitLab CI" (!89)
  - doc: Add missing usage example in "Using packages" (!89)

**Fixes:**

- Use api param namespace_id to parent_id for subgroup creation (!91)

## 0.7.0 (2020-10-29)

**New features:**

- Implement group of groups (!74)
- Implement pipeline schedules (including variables) (!75)
- Add aggregate merge strategy (!77)

**Enhancements:**

- Document required release dependencies (!72)
- Update copyright to 2020 (!73)
- Fix Unknown merge strategy error message (!76)
- Consider null label description as same as empty (!78)
- Ship all documentation in deb package (!79)

## 0.6.0 (2020-06-18)

**Important fixes:**

- **Security**: Mirroring: Only send Private-Token on gitlab remote (!63)

**New features:**

- Implement project's webhooks (!67)

**Enhancements:**

- Add forking\_access\_level, pages\_access\_level, emails\_disabled and suggestion\_commit\_message
  to projects (!66, !69)
- Add support for project's allow\_merge\_on\_skipped\_pipeline and autoclose\_referenced\_issues (!70)
- tests: Ignore host and port on match (!67)
- Documentation:
  - Reorder project and group parameters as in WebUI (!65)
  - Update release doc (!62)
  - Sync README with requirements.txt (pygit2 is now needed) (!69)

**Fixes:**

- Pin pygit2 to 1.0.3 to fix FTBFS (!64)
- CI and tests fixes (!68):
  - Fix "jobs:dast config key may not be used with `rules`: except"
  - Fix "E712 comparison to False should be 'if cond is False:' or 'if not cond:'"
  - Fix "E302 expected 2 blank lines, found 1"

## 0.5.0 (2020-01-23)

**Backwards incompatible changes:**

- `extends` now use `deep` merging by default, to restore the previous behavior
  use `replace` merge strategy

**New features:**

- `extends` now supports two merge strategies: `deep` (the default) and `replace` (!54 and !56)
- `extends` now supports multiple parents (!56)
- Repository mirroring (!52, !60)
- Add support for project and group milestones (!58)

**Enhancements:**

- Add remove\_source\_branch\_after\_merge parameter to projects (!46)
- Add default\_ci\_config\_path parameter to application settings (!47)
- Add emails\_disabled, project\_creation\_level, subgroup\_creation\_level, require\_two\_factor\_authentication,
  two\_factor\_grace\_period, auto\_devops\_enabled to groups (!59)
- Sync with current project and group parameters (!59)
- Documentation:
  - Add Table of Contents (!48)
  - Generate markdown doc from source (!48)
  - Document releasing a new version (!49)
  - Various documentation enhancements (!51)
  - Add general action file documentation (!53)
- Tests:
  - Only install packages for the gitlabracadabra.tests.unit package (!55)

**Fixes:**

- User:
  - Fix typo of user param: `s/admin/is_admin/` (!57)
  - Remove username as param, as this is the id (!57)
  - Fix `CREATE_KEY` handling (!57)
  - user's skip_confirmation is a create param (!57)
  - Do not try to change create-only params for users (!57)
- Tests:
  - Clean up before testing group member not found (!50)

## 0.4.0 (2019-11-14)

**New features:**

- Add delete\_object param (!43)
- Add support for include (#7, !42)
- Add project and group labels support (#2, !36, !39, !40)
- Add support for application settings (#4, !34)
- Package uploaded to Debian (!45)

**Enhancements:**

- Add support for latest project params (!33)
- Test YAML file loading (!42)
- Ensure that content is not changed during processing (!41)
- User password is only set on user creation (!41)
- Assert all cassettes are "all played" (!37, !38)

**Fixes:**

- Don't fail when some variables params are not available (!44)
- Python-gitlab 1.13 still has incorrect labels support (!45)

## 0.2.1 (2019-06-20)

**New features:**

- Debian packages (!24, !25)
- Project and group variables (!29)
- Project archived parameter (!27)

**Enhancements:**

- Catch members deletion errors (!30)
- Skip changing parameter only when not available (i.e. handle null as current value) (!28)
- Catch REST errors of \_process\_ methods (!26)
- Enforce draft 4 of jsonschema (!25)
- Skip protected tags tests on python-gitlab < 1.7.0 (!24)
- Workaround "all=True" in API calls with python-gitlab < 1.8.0 (!24)
- Move VCR fixtures closer to the tests (!23)
- Centralize VCR configuration (!23)

## 0.1.0 (2019-05-25)

**New features:**

- Projects
  - Basic parameters
  - Project members
  - Add protected branches support
  - Protected tags
  - Add latest project parameters
    (mirror\_user\_id, only\_mirror\_protected\_branches, mirror\_overwrites\_diverged\_branches, packages\_enabled)
  - Add initialize\_with\_readme support to projects
- Groups:
  - Basic parameters
  - Group members
  - Add latest group parameters
    (membership\_lock, share\_with\_group\_lock, file\_template\_project\_id, extra\_shared\_runners\_minutes\_limit)
- Users:
  - Basic parameters
