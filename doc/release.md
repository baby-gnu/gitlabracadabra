# Releasing GitLabracadabra

## Requirements

Ensure last pipeline for `main` passed.

Ensure required dependencies are installed:

```shell
sudo apt-get install -y devscripts dh-python git-buildpackage python3-virtualenv
npm install \
  semantic-release \
  @semantic-release/changelog \
  @semantic-release/exec \
  @semantic-release/git \
  conventional-changelog-conventionalcommits \
  @google/semantic-release-replace-plugin
```

Ensure you can push to `main` branch.

## Pre-release tests

Test the build:

```shell
rm -rf .tox
tox

rm -rf venv
virtualenv venv
. venv/bin/activate
pip install -r requirements.txt -r test-requirements.txt
python3 setup.py build
python3 setup.py install
pytest
```

Re-generate the doc:

```shell
for t in project group user application_settings; do
  gitlabracadabra --doc-markdown $t > doc/$t.md
done
```

## Release

Ensure you're up to date:

```shell
git checkout main
git pull --rebase
git status
```

Run `semantic-release`:

```shell
npx semantic-release --no-ci
```

Build the pip package:

```shell
pip install wheel twine
python3 setup.py sdist bdist_wheel
```

Build the Debian package:

```shell
gbp buildpackage -S -d
```

<!--
Upload to test.pypi.org

```shell
twine upload --repository-url https://test.pypi.org/legacy/ "dist/gitlabracadabra-$target"*
```
-->

Upload artifacts to PyPI and Debian:

```shell
version="$(grep __version__ gitlabracadabra/__init__.py  | awk -F "'" '{print $2}')"
twine upload "dist/gitlabracadabra-$version"*

dput "../gitlabracadabra_${version}_source.changes"
```
