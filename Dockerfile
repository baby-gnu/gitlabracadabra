FROM debian:bullseye

COPY pygit2-proxy.patch urllib3-Lower-retry-message-from-WARNING-to-INFO.patch /tmp/

# DL3008 Pin versions in apt get install
# hadolint ignore=DL3008
RUN set -x && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
      patch \
      python3 \
      python3-setuptools \
      python3-jsonschema \
      python3-github \
      python3-gitlab \
      python3-yaml \
      python3-pygit2 \
      python3-semantic-version \
      \
      wget \
    && wget --progress=dot:giga https://salsa.debian.org/debian/libgit2/-/jobs/1435734/artifacts/raw/debian/output/libgit2-1.1_1.1.0+dfsg.1-4+noproxy1+salsaci_amd64.deb \
    && apt-get install -y --no-install-recommends \
       ./libgit2-1.1_*.deb \
    && patch /usr/lib/python3/dist-packages/pygit2/remote.py < /tmp/pygit2-proxy.patch \
    && patch /usr/lib/python3/dist-packages/urllib3/connectionpool.py < /tmp/urllib3-Lower-retry-message-from-WARNING-to-INFO.patch \
    && rm -v /tmp/*.patch \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app
COPY . /app

RUN python3 setup.py build && \
    python3 setup.py install && \
    adduser --disabled-password --gecos '' gitlabracadabra

USER gitlabracadabra

ENTRYPOINT ["gitlabracadabra"]
